package com.kgc.cn.dto;

import java.util.ArrayList;
import java.util.List;

public class HouseleaseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HouseleaseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHouseIdIsNull() {
            addCriterion("houseId is null");
            return (Criteria) this;
        }

        public Criteria andHouseIdIsNotNull() {
            addCriterion("houseId is not null");
            return (Criteria) this;
        }

        public Criteria andHouseIdEqualTo(String value) {
            addCriterion("houseId =", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotEqualTo(String value) {
            addCriterion("houseId <>", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThan(String value) {
            addCriterion("houseId >", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdGreaterThanOrEqualTo(String value) {
            addCriterion("houseId >=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThan(String value) {
            addCriterion("houseId <", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLessThanOrEqualTo(String value) {
            addCriterion("houseId <=", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdLike(String value) {
            addCriterion("houseId like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotLike(String value) {
            addCriterion("houseId not like", value, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdIn(List<String> values) {
            addCriterion("houseId in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotIn(List<String> values) {
            addCriterion("houseId not in", values, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdBetween(String value1, String value2) {
            addCriterion("houseId between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseIdNotBetween(String value1, String value2) {
            addCriterion("houseId not between", value1, value2, "houseId");
            return (Criteria) this;
        }

        public Criteria andHouseAddressIsNull() {
            addCriterion("houseAddress is null");
            return (Criteria) this;
        }

        public Criteria andHouseAddressIsNotNull() {
            addCriterion("houseAddress is not null");
            return (Criteria) this;
        }

        public Criteria andHouseAddressEqualTo(String value) {
            addCriterion("houseAddress =", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressNotEqualTo(String value) {
            addCriterion("houseAddress <>", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressGreaterThan(String value) {
            addCriterion("houseAddress >", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressGreaterThanOrEqualTo(String value) {
            addCriterion("houseAddress >=", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressLessThan(String value) {
            addCriterion("houseAddress <", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressLessThanOrEqualTo(String value) {
            addCriterion("houseAddress <=", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressLike(String value) {
            addCriterion("houseAddress like", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressNotLike(String value) {
            addCriterion("houseAddress not like", value, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressIn(List<String> values) {
            addCriterion("houseAddress in", values, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressNotIn(List<String> values) {
            addCriterion("houseAddress not in", values, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressBetween(String value1, String value2) {
            addCriterion("houseAddress between", value1, value2, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseAddressNotBetween(String value1, String value2) {
            addCriterion("houseAddress not between", value1, value2, "houseAddress");
            return (Criteria) this;
        }

        public Criteria andHouseNameIsNull() {
            addCriterion("houseName is null");
            return (Criteria) this;
        }

        public Criteria andHouseNameIsNotNull() {
            addCriterion("houseName is not null");
            return (Criteria) this;
        }

        public Criteria andHouseNameEqualTo(String value) {
            addCriterion("houseName =", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameNotEqualTo(String value) {
            addCriterion("houseName <>", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameGreaterThan(String value) {
            addCriterion("houseName >", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameGreaterThanOrEqualTo(String value) {
            addCriterion("houseName >=", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameLessThan(String value) {
            addCriterion("houseName <", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameLessThanOrEqualTo(String value) {
            addCriterion("houseName <=", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameLike(String value) {
            addCriterion("houseName like", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameNotLike(String value) {
            addCriterion("houseName not like", value, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameIn(List<String> values) {
            addCriterion("houseName in", values, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameNotIn(List<String> values) {
            addCriterion("houseName not in", values, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameBetween(String value1, String value2) {
            addCriterion("houseName between", value1, value2, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseNameNotBetween(String value1, String value2) {
            addCriterion("houseName not between", value1, value2, "houseName");
            return (Criteria) this;
        }

        public Criteria andHouseAreaIsNull() {
            addCriterion("houseArea is null");
            return (Criteria) this;
        }

        public Criteria andHouseAreaIsNotNull() {
            addCriterion("houseArea is not null");
            return (Criteria) this;
        }

        public Criteria andHouseAreaEqualTo(String value) {
            addCriterion("houseArea =", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaNotEqualTo(String value) {
            addCriterion("houseArea <>", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaGreaterThan(String value) {
            addCriterion("houseArea >", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaGreaterThanOrEqualTo(String value) {
            addCriterion("houseArea >=", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaLessThan(String value) {
            addCriterion("houseArea <", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaLessThanOrEqualTo(String value) {
            addCriterion("houseArea <=", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaLike(String value) {
            addCriterion("houseArea like", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaNotLike(String value) {
            addCriterion("houseArea not like", value, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaIn(List<String> values) {
            addCriterion("houseArea in", values, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaNotIn(List<String> values) {
            addCriterion("houseArea not in", values, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaBetween(String value1, String value2) {
            addCriterion("houseArea between", value1, value2, "houseArea");
            return (Criteria) this;
        }

        public Criteria andHouseAreaNotBetween(String value1, String value2) {
            addCriterion("houseArea not between", value1, value2, "houseArea");
            return (Criteria) this;
        }

        public Criteria andFloorIsNull() {
            addCriterion("`floor` is null");
            return (Criteria) this;
        }

        public Criteria andFloorIsNotNull() {
            addCriterion("`floor` is not null");
            return (Criteria) this;
        }

        public Criteria andFloorEqualTo(Integer value) {
            addCriterion("`floor` =", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorNotEqualTo(Integer value) {
            addCriterion("`floor` <>", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorGreaterThan(Integer value) {
            addCriterion("`floor` >", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorGreaterThanOrEqualTo(Integer value) {
            addCriterion("`floor` >=", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorLessThan(Integer value) {
            addCriterion("`floor` <", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorLessThanOrEqualTo(Integer value) {
            addCriterion("`floor` <=", value, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorIn(List<Integer> values) {
            addCriterion("`floor` in", values, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorNotIn(List<Integer> values) {
            addCriterion("`floor` not in", values, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorBetween(Integer value1, Integer value2) {
            addCriterion("`floor` between", value1, value2, "floor");
            return (Criteria) this;
        }

        public Criteria andFloorNotBetween(Integer value1, Integer value2) {
            addCriterion("`floor` not between", value1, value2, "floor");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdIsNull() {
            addCriterion("decorateSituationId is null");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdIsNotNull() {
            addCriterion("decorateSituationId is not null");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdEqualTo(Integer value) {
            addCriterion("decorateSituationId =", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdNotEqualTo(Integer value) {
            addCriterion("decorateSituationId <>", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdGreaterThan(Integer value) {
            addCriterion("decorateSituationId >", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("decorateSituationId >=", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdLessThan(Integer value) {
            addCriterion("decorateSituationId <", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdLessThanOrEqualTo(Integer value) {
            addCriterion("decorateSituationId <=", value, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdIn(List<Integer> values) {
            addCriterion("decorateSituationId in", values, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdNotIn(List<Integer> values) {
            addCriterion("decorateSituationId not in", values, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdBetween(Integer value1, Integer value2) {
            addCriterion("decorateSituationId between", value1, value2, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andDecorateSituationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("decorateSituationId not between", value1, value2, "decorateSituationId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdIsNull() {
            addCriterion("leaseId is null");
            return (Criteria) this;
        }

        public Criteria andLeaseIdIsNotNull() {
            addCriterion("leaseId is not null");
            return (Criteria) this;
        }

        public Criteria andLeaseIdEqualTo(Integer value) {
            addCriterion("leaseId =", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdNotEqualTo(Integer value) {
            addCriterion("leaseId <>", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdGreaterThan(Integer value) {
            addCriterion("leaseId >", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("leaseId >=", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdLessThan(Integer value) {
            addCriterion("leaseId <", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdLessThanOrEqualTo(Integer value) {
            addCriterion("leaseId <=", value, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdIn(List<Integer> values) {
            addCriterion("leaseId in", values, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdNotIn(List<Integer> values) {
            addCriterion("leaseId not in", values, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdBetween(Integer value1, Integer value2) {
            addCriterion("leaseId between", value1, value2, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("leaseId not between", value1, value2, "leaseId");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyIsNull() {
            addCriterion("leaseMoney is null");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyIsNotNull() {
            addCriterion("leaseMoney is not null");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyEqualTo(Integer value) {
            addCriterion("leaseMoney =", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyNotEqualTo(Integer value) {
            addCriterion("leaseMoney <>", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyGreaterThan(Integer value) {
            addCriterion("leaseMoney >", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyGreaterThanOrEqualTo(Integer value) {
            addCriterion("leaseMoney >=", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyLessThan(Integer value) {
            addCriterion("leaseMoney <", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyLessThanOrEqualTo(Integer value) {
            addCriterion("leaseMoney <=", value, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyIn(List<Integer> values) {
            addCriterion("leaseMoney in", values, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyNotIn(List<Integer> values) {
            addCriterion("leaseMoney not in", values, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyBetween(Integer value1, Integer value2) {
            addCriterion("leaseMoney between", value1, value2, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLeaseMoneyNotBetween(Integer value1, Integer value2) {
            addCriterion("leaseMoney not between", value1, value2, "leaseMoney");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameIsNull() {
            addCriterion("linkmanName is null");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameIsNotNull() {
            addCriterion("linkmanName is not null");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameEqualTo(String value) {
            addCriterion("linkmanName =", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameNotEqualTo(String value) {
            addCriterion("linkmanName <>", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameGreaterThan(String value) {
            addCriterion("linkmanName >", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameGreaterThanOrEqualTo(String value) {
            addCriterion("linkmanName >=", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameLessThan(String value) {
            addCriterion("linkmanName <", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameLessThanOrEqualTo(String value) {
            addCriterion("linkmanName <=", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameLike(String value) {
            addCriterion("linkmanName like", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameNotLike(String value) {
            addCriterion("linkmanName not like", value, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameIn(List<String> values) {
            addCriterion("linkmanName in", values, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameNotIn(List<String> values) {
            addCriterion("linkmanName not in", values, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameBetween(String value1, String value2) {
            addCriterion("linkmanName between", value1, value2, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanNameNotBetween(String value1, String value2) {
            addCriterion("linkmanName not between", value1, value2, "linkmanName");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexIsNull() {
            addCriterion("linkmanSex is null");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexIsNotNull() {
            addCriterion("linkmanSex is not null");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexEqualTo(Integer value) {
            addCriterion("linkmanSex =", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexNotEqualTo(Integer value) {
            addCriterion("linkmanSex <>", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexGreaterThan(Integer value) {
            addCriterion("linkmanSex >", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexGreaterThanOrEqualTo(Integer value) {
            addCriterion("linkmanSex >=", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexLessThan(Integer value) {
            addCriterion("linkmanSex <", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexLessThanOrEqualTo(Integer value) {
            addCriterion("linkmanSex <=", value, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexIn(List<Integer> values) {
            addCriterion("linkmanSex in", values, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexNotIn(List<Integer> values) {
            addCriterion("linkmanSex not in", values, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexBetween(Integer value1, Integer value2) {
            addCriterion("linkmanSex between", value1, value2, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andLinkmanSexNotBetween(Integer value1, Integer value2) {
            addCriterion("linkmanSex not between", value1, value2, "linkmanSex");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("userId =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("userId <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("userId >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userId >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("userId <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("userId <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("userId in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("userId not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("userId between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userId not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeIsNull() {
            addCriterion("lookHouseTime is null");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeIsNotNull() {
            addCriterion("lookHouseTime is not null");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeEqualTo(String value) {
            addCriterion("lookHouseTime =", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeNotEqualTo(String value) {
            addCriterion("lookHouseTime <>", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeGreaterThan(String value) {
            addCriterion("lookHouseTime >", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeGreaterThanOrEqualTo(String value) {
            addCriterion("lookHouseTime >=", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeLessThan(String value) {
            addCriterion("lookHouseTime <", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeLessThanOrEqualTo(String value) {
            addCriterion("lookHouseTime <=", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeLike(String value) {
            addCriterion("lookHouseTime like", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeNotLike(String value) {
            addCriterion("lookHouseTime not like", value, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeIn(List<String> values) {
            addCriterion("lookHouseTime in", values, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeNotIn(List<String> values) {
            addCriterion("lookHouseTime not in", values, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeBetween(String value1, String value2) {
            addCriterion("lookHouseTime between", value1, value2, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLookHouseTimeNotBetween(String value1, String value2) {
            addCriterion("lookHouseTime not between", value1, value2, "lookHouseTime");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberIsNull() {
            addCriterion("leaseHouseNumber is null");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberIsNotNull() {
            addCriterion("leaseHouseNumber is not null");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberEqualTo(Integer value) {
            addCriterion("leaseHouseNumber =", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberNotEqualTo(Integer value) {
            addCriterion("leaseHouseNumber <>", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberGreaterThan(Integer value) {
            addCriterion("leaseHouseNumber >", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("leaseHouseNumber >=", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberLessThan(Integer value) {
            addCriterion("leaseHouseNumber <", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberLessThanOrEqualTo(Integer value) {
            addCriterion("leaseHouseNumber <=", value, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberIn(List<Integer> values) {
            addCriterion("leaseHouseNumber in", values, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberNotIn(List<Integer> values) {
            addCriterion("leaseHouseNumber not in", values, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberBetween(Integer value1, Integer value2) {
            addCriterion("leaseHouseNumber between", value1, value2, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseHouseNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("leaseHouseNumber not between", value1, value2, "leaseHouseNumber");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeIsNull() {
            addCriterion("leaseStartTime is null");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeIsNotNull() {
            addCriterion("leaseStartTime is not null");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeEqualTo(String value) {
            addCriterion("leaseStartTime =", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeNotEqualTo(String value) {
            addCriterion("leaseStartTime <>", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeGreaterThan(String value) {
            addCriterion("leaseStartTime >", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("leaseStartTime >=", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeLessThan(String value) {
            addCriterion("leaseStartTime <", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeLessThanOrEqualTo(String value) {
            addCriterion("leaseStartTime <=", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeLike(String value) {
            addCriterion("leaseStartTime like", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeNotLike(String value) {
            addCriterion("leaseStartTime not like", value, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeIn(List<String> values) {
            addCriterion("leaseStartTime in", values, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeNotIn(List<String> values) {
            addCriterion("leaseStartTime not in", values, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeBetween(String value1, String value2) {
            addCriterion("leaseStartTime between", value1, value2, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andLeaseStartTimeNotBetween(String value1, String value2) {
            addCriterion("leaseStartTime not between", value1, value2, "leaseStartTime");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesIsNull() {
            addCriterion("homeAppliances is null");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesIsNotNull() {
            addCriterion("homeAppliances is not null");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesEqualTo(String value) {
            addCriterion("homeAppliances =", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesNotEqualTo(String value) {
            addCriterion("homeAppliances <>", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesGreaterThan(String value) {
            addCriterion("homeAppliances >", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesGreaterThanOrEqualTo(String value) {
            addCriterion("homeAppliances >=", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesLessThan(String value) {
            addCriterion("homeAppliances <", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesLessThanOrEqualTo(String value) {
            addCriterion("homeAppliances <=", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesLike(String value) {
            addCriterion("homeAppliances like", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesNotLike(String value) {
            addCriterion("homeAppliances not like", value, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesIn(List<String> values) {
            addCriterion("homeAppliances in", values, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesNotIn(List<String> values) {
            addCriterion("homeAppliances not in", values, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesBetween(String value1, String value2) {
            addCriterion("homeAppliances between", value1, value2, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHomeAppliancesNotBetween(String value1, String value2) {
            addCriterion("homeAppliances not between", value1, value2, "homeAppliances");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitIsNull() {
            addCriterion("houseBenefit is null");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitIsNotNull() {
            addCriterion("houseBenefit is not null");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitEqualTo(String value) {
            addCriterion("houseBenefit =", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitNotEqualTo(String value) {
            addCriterion("houseBenefit <>", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitGreaterThan(String value) {
            addCriterion("houseBenefit >", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitGreaterThanOrEqualTo(String value) {
            addCriterion("houseBenefit >=", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitLessThan(String value) {
            addCriterion("houseBenefit <", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitLessThanOrEqualTo(String value) {
            addCriterion("houseBenefit <=", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitLike(String value) {
            addCriterion("houseBenefit like", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitNotLike(String value) {
            addCriterion("houseBenefit not like", value, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitIn(List<String> values) {
            addCriterion("houseBenefit in", values, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitNotIn(List<String> values) {
            addCriterion("houseBenefit not in", values, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitBetween(String value1, String value2) {
            addCriterion("houseBenefit between", value1, value2, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andHouseBenefitNotBetween(String value1, String value2) {
            addCriterion("houseBenefit not between", value1, value2, "houseBenefit");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireIsNull() {
            addCriterion("leaseRequire is null");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireIsNotNull() {
            addCriterion("leaseRequire is not null");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireEqualTo(String value) {
            addCriterion("leaseRequire =", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireNotEqualTo(String value) {
            addCriterion("leaseRequire <>", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireGreaterThan(String value) {
            addCriterion("leaseRequire >", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireGreaterThanOrEqualTo(String value) {
            addCriterion("leaseRequire >=", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireLessThan(String value) {
            addCriterion("leaseRequire <", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireLessThanOrEqualTo(String value) {
            addCriterion("leaseRequire <=", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireLike(String value) {
            addCriterion("leaseRequire like", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireNotLike(String value) {
            addCriterion("leaseRequire not like", value, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireIn(List<String> values) {
            addCriterion("leaseRequire in", values, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireNotIn(List<String> values) {
            addCriterion("leaseRequire not in", values, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireBetween(String value1, String value2) {
            addCriterion("leaseRequire between", value1, value2, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andLeaseRequireNotBetween(String value1, String value2) {
            addCriterion("leaseRequire not between", value1, value2, "leaseRequire");
            return (Criteria) this;
        }

        public Criteria andCommentIsNull() {
            addCriterion("`comment` is null");
            return (Criteria) this;
        }

        public Criteria andCommentIsNotNull() {
            addCriterion("`comment` is not null");
            return (Criteria) this;
        }

        public Criteria andCommentEqualTo(String value) {
            addCriterion("`comment` =", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotEqualTo(String value) {
            addCriterion("`comment` <>", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThan(String value) {
            addCriterion("`comment` >", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThanOrEqualTo(String value) {
            addCriterion("`comment` >=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThan(String value) {
            addCriterion("`comment` <", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThanOrEqualTo(String value) {
            addCriterion("`comment` <=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLike(String value) {
            addCriterion("`comment` like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotLike(String value) {
            addCriterion("`comment` not like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentIn(List<String> values) {
            addCriterion("`comment` in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotIn(List<String> values) {
            addCriterion("`comment` not in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentBetween(String value1, String value2) {
            addCriterion("`comment` between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotBetween(String value1, String value2) {
            addCriterion("`comment` not between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andIsJointRentIsNull() {
            addCriterion("isJointRent is null");
            return (Criteria) this;
        }

        public Criteria andIsJointRentIsNotNull() {
            addCriterion("isJointRent is not null");
            return (Criteria) this;
        }

        public Criteria andIsJointRentEqualTo(Integer value) {
            addCriterion("isJointRent =", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentNotEqualTo(Integer value) {
            addCriterion("isJointRent <>", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentGreaterThan(Integer value) {
            addCriterion("isJointRent >", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentGreaterThanOrEqualTo(Integer value) {
            addCriterion("isJointRent >=", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentLessThan(Integer value) {
            addCriterion("isJointRent <", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentLessThanOrEqualTo(Integer value) {
            addCriterion("isJointRent <=", value, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentIn(List<Integer> values) {
            addCriterion("isJointRent in", values, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentNotIn(List<Integer> values) {
            addCriterion("isJointRent not in", values, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentBetween(Integer value1, Integer value2) {
            addCriterion("isJointRent between", value1, value2, "isJointRent");
            return (Criteria) this;
        }

        public Criteria andIsJointRentNotBetween(Integer value1, Integer value2) {
            addCriterion("isJointRent not between", value1, value2, "isJointRent");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}