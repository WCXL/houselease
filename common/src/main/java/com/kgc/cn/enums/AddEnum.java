package com.kgc.cn.enums;

/**
 * Created by boot on 2020/1/8
 */
public enum AddEnum {

    ADD_ERROR(522, "添加数据库失败"),
    NONE_INCOMPLETEINFO(523, "没有未完成信息");

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    AddEnum() {
    }

    AddEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
