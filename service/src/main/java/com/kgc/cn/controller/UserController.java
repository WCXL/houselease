package com.kgc.cn.controller;

import com.kgc.cn.dto.Enshrine;
import com.kgc.cn.dto.EnshrineDto;
import com.kgc.cn.dto.User;
import com.kgc.cn.service.Impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Ding on 2020/1/7.
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserServiceImpl userService;

    @PostMapping("/getUser")
    public User getUser(@RequestParam("phone") String phone) {
        return userService.getUser(phone);
    }

}
