package com.kgc.cn.mapper;

import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.HomeappliancesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HomeappliancesMapper {
    long countByExample(HomeappliancesExample example);

    int deleteByExample(HomeappliancesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Homeappliances record);

    int insertSelective(Homeappliances record);

    List<Homeappliances> selectByExample(HomeappliancesExample example);

    Homeappliances selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Homeappliances record, @Param("example") HomeappliancesExample example);

    int updateByExample(@Param("record") Homeappliances record, @Param("example") HomeappliancesExample example);

    int updateByPrimaryKeySelective(Homeappliances record);

    int updateByPrimaryKey(Homeappliances record);
}