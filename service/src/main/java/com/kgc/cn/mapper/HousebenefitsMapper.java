package com.kgc.cn.mapper;

import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.HousebenefitsExample;
import java.util.List;

import com.kgc.cn.dto.Leaserequires;
import org.apache.ibatis.annotations.Param;

public interface HousebenefitsMapper {
    long countByExample(HousebenefitsExample example);

    int deleteByExample(HousebenefitsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Housebenefits record);

    int insertSelective(Housebenefits record);

    List<Housebenefits> selectByExample(HousebenefitsExample example);

    Housebenefits selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Housebenefits record, @Param("example") HousebenefitsExample example);

    int updateByExample(@Param("record") Housebenefits record, @Param("example") HousebenefitsExample example);

    int updateByPrimaryKeySelective(Housebenefits record);

    int updateByPrimaryKey(Housebenefits record);

    //返回房源特色列表
    List<Housebenefits> housebenefitsList();
}