package com.kgc.cn.mapper;

import java.util.List;

import com.kgc.cn.dto.Orientations;
import com.kgc.cn.dto.OrientationsExample;
import org.apache.ibatis.annotations.Param;

public interface OrientationsMapper {
    long countByExample(OrientationsExample example);

    int deleteByExample(OrientationsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Orientations record);

    int insertSelective(Orientations record);

    List<Orientations> selectByExample(OrientationsExample example);

    Orientations selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Orientations record, @Param("example") OrientationsExample example);

    int updateByExample(@Param("record") Orientations record, @Param("example") OrientationsExample example);

    int updateByPrimaryKeySelective(Orientations record);

    int updateByPrimaryKey(Orientations record);

    List<Orientations> orientationsList();
}