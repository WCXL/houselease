package com.kgc.cn.service.Impl;

import com.kgc.cn.dto.*;
import com.kgc.cn.mapper.*;
import com.kgc.cn.dto.*;
import com.kgc.cn.mapper.*;
import com.kgc.cn.dto.Addresses;
import com.kgc.cn.service.AddLeaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Service
public class AddLeaseServiceImpl implements AddLeaseService {
    @Autowired
    private HouseleaseMapper houseleaseMapper;
    @Autowired
    private IsjointrentMapper isjointrentMapper;
    @Autowired
    private NojointrentMapper nojointrentMapper;
    @Autowired
    private HomeappliancesMapper homeappliancesMapper;
    @Autowired
    private HousebenefitsMapper housebenefitsMapper;
    @Autowired
    private LeaserequiresMapper leaserequiresMapper;

    @Autowired
    private DecoratesituationMapper decoratesituationMapper;
    @Autowired
    private LessesMapper lessesMapper;
    /**
     * 添加整租的NoJoinHouselease和Nojointrent
     *
     * @param houseLeaseDto
     * @return
     */
    @Transactional
    @Override
    public int addNojoin(HouseLeaseDto houseLeaseDto) {
        Houselease nojoinHouselease = new Houselease();
        Nojointrent nojointrent = new Nojointrent();
        BeanUtils.copyProperties(houseLeaseDto, nojoinHouselease);
        BeanUtils.copyProperties(houseLeaseDto, nojointrent);
        nojoinHouselease.setIsJointRent(0);
        int flagA = houseleaseMapper.insert(nojoinHouselease);
        int flagB = nojointrentMapper.insert(nojointrent);
        if (flagA == 1 && flagB == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 展示可选房屋配置
     *
     * @return
     */
    @Override
    public List<Homeappliances> showAllocation() {
        HomeappliancesExample example = new HomeappliancesExample();
        return homeappliancesMapper.selectByExample(example);
    }

    /**
     * 展示房屋亮点
     *
     * @return
     */
    @Override
    public List<Housebenefits> showPoint() {
        HousebenefitsExample example = new HousebenefitsExample();
        return housebenefitsMapper.selectByExample(example);
    }

    /**
     * 展示出租要求
     *
     * @return
     */
    @Override
    public List<Leaserequires> showRequirement() {
        LeaserequiresExample example = new LeaserequiresExample();
        return leaserequiresMapper.selectByExample(example);
    }

    /**
     * 合租
     *
     * @param rentSharingParam
     * @return
     */
    @Override
    public String addRentSharing(houseLeaseIsJointDto isJointDto) {
        Houselease houselease = new Houselease();
        Isjointrent isjointrent = new Isjointrent();
        //租房信息
        BeanUtils.copyProperties(isJointDto, houselease);
        int flag = houseleaseMapper.insertLease(houselease);
        //合租
        BeanUtils.copyProperties(isJointDto, isjointrent);
        int flagisjointrent = isjointrentMapper.insertIsJointRent(isjointrent);
        if (flag > 0 && flagisjointrent > 0) {
            return "success";
        }
        return null;
    }

    /**
     * 地区查找
     * @param houseAddress
     * @return
     */
    @Override
    public List<Decoratesituation> querydecorateSituation() {
        return decoratesituationMapper.querydecorateSituation();
    }

    /**
     * 合租
     */
    @Override
    public List<Lesses> queryShortRent() {
        return lessesMapper.queryShortRent();
    }

}
