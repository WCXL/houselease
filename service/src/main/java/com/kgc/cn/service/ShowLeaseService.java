package com.kgc.cn.service;

import com.kgc.cn.dto.*;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
public interface ShowLeaseService {
    ShowLeaseDto showLease(String houseId);

    // 查找所有共同租房信息
    List<Houselease> houseLease(int current, int size);

    // 查找所有租房信息条数
    int houseLeaseNum();

    // 查找所有合租信息
    List<Isjointrent> isJoin();

    // 查找所有整租信息
    List<Nojointrent> noJoin();

    // 查找地区所有共同租房信息
    List<Houselease> houseLeaseByAddress(String houseArea, int current, int size);

    // 根据小区地址和小区名称模糊搜索
    public List<Houselease> searchByAddAndName(String search, int current, int size);

    //查询用户下所有的租赁信息
    List<ShowLeaseDto> showLeaseByUserId(String userId);

    // 模糊搜索
    List<ShowLeaseDto> fuzzySearch(fuzzyHouseDto fuzzyHouseDto,int sort);

    //获得轮播图
    List<Pictures> getPicture();

    // 返回地区列表
    List<Addresses> addressList();

    // 返回租金列表
    List<Rells> rellsList();

    // 返回户型列表
    List<Bedrooms> bedroomsList();

    // 返回朝向列表
    List<Orientations> orientationsList();

    // 返回出租要求列表
    List<Leaserequires> leaserequiresList();

    // 返回房源特色列表
    List<Housebenefits> housebenefitsList();
}
