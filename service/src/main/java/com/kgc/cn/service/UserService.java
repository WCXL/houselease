package com.kgc.cn.service;

import com.kgc.cn.dto.User;

/**
 * Created by Ding on 2020/1/7.
 */
public interface UserService {
    //通过手机号获得对用用户
    User getUser(String phone);
}
