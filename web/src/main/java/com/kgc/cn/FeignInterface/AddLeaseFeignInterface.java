package com.kgc.cn.FeignInterface;

import com.kgc.cn.FeignInterface.Impl.AddLeaseFeignImpl;
import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.HouseLeaseDto;
import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.Leaserequires;
import com.kgc.cn.param.RentSharingParam;
import com.kgc.cn.dto.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@FeignClient(name = "service", fallback = AddLeaseFeignImpl.class)
public interface AddLeaseFeignInterface {

    // 添加整租
    @PostMapping(value = "/AddLease/addNo")
    int addNoJoinTrent(@RequestBody HouseLeaseDto houseLeaseDto);

    // 显示房屋配置
    @GetMapping(value = "/AddLease/showAllocation")
    List<Homeappliances> showHouseAllocation();

    // 显示房屋亮点
    @GetMapping(value = "/AddLease/showPoint")
    List<Housebenefits> showBrightPoint();

    // 显示出租要求
    @GetMapping(value = "/AddLease/showRequirement")
    List<Leaserequires> showRequirement();

    //合租
    @PostMapping(value = "/AddLease/RentSharing")
    String addRentSharing(@RequestBody houseLeaseIsJointDto houseLeaseIsJointDto);

    //地区查找
    @PostMapping(value = "/AddLease/queryAddress")
    List<Decoratesituation> querydecorateSituation();

    //短租
    @PostMapping(value = "/AddLease/queryShortRent")
    List<Lesses> queryShortRent();
}
