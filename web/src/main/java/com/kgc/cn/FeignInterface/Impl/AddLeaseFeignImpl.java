package com.kgc.cn.FeignInterface.Impl;

import com.kgc.cn.FeignInterface.AddLeaseFeignInterface;
import com.kgc.cn.dto.Homeappliances;
import com.kgc.cn.dto.HouseLeaseDto;
import com.kgc.cn.dto.Housebenefits;
import com.kgc.cn.dto.Leaserequires;
import com.kgc.cn.dto.*;
import com.kgc.cn.param.RentSharingParam;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Component
public class AddLeaseFeignImpl implements AddLeaseFeignInterface {
    @Override
    public int addNoJoinTrent(HouseLeaseDto houseLeaseDto) {
        return 0;
    }

    @Override
    public List<Homeappliances> showHouseAllocation() {
        return null;
    }

    @Override
    public List<Housebenefits> showBrightPoint() {
        return null;
    }

    @Override
    public List<Leaserequires> showRequirement() {
        return null;
    }

    @Override
    public String addRentSharing(houseLeaseIsJointDto houseLeaseIsJointDto) {
        return "webNull";
    }

    @Override
    public List<Decoratesituation> querydecorateSituation() {
        return null;
    }

    @Override
    public List<Lesses> queryShortRent() {
        return null;
    }
}
