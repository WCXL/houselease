package com.kgc.cn.controller;

import com.kgc.cn.enums.UserEnum;
import com.kgc.cn.param.EnshrineParam;
import com.kgc.cn.service.UserService;
import com.kgc.cn.utils.returnResult.ReturnResult;
import com.kgc.cn.utils.returnResult.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Ding on 2020/1/7.
 */
@Api(tags = "用户操作")
@RestController
@RequestMapping("/webUser")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 用户登录
     *
     * @param phone
     * @param passWord
     * @return
     */
    @ApiOperation("登录")
    @GetMapping("/login")
    public ReturnResult<String> login(@ApiParam("手机号") @RequestParam String phone,
                              @ApiParam("密码") @RequestParam String passWord,
                              HttpServletRequest request) {
        String flag = userService.login(phone, passWord, request);
        if (!flag.equals("false")) {
            return ReturnResultUtils.returnSuccess(flag);
        }
        return ReturnResultUtils.returnFail(UserEnum.LOGIN_ERROR);
    }

    /**
     * 展示收藏
     * @param userId
     * @return
     */
    @ApiOperation("展示收藏")
    @PostMapping(value = "/showEnshrine")
    public ReturnResult<List<EnshrineParam>> showEnshrine(@ApiParam("用户id") @RequestParam String userId){
        List<EnshrineParam> enshrineParamList = userService.showEnshrine(userId);
        if (!CollectionUtils.isEmpty(enshrineParamList)){
            return ReturnResultUtils.returnSuccess(enshrineParamList);
        }
        return ReturnResultUtils.returnFail(UserEnum.SHOW_ENSHRINE);

    }
}