package com.kgc.cn.param;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;

/**
 * @auther zhouxinyu
 * @data 2020/1/9
 */
@Data
@ApiOperation(value = "房源亮点model")
public class HousebenefitsParam implements Serializable {
    @ApiModelProperty(value = "房源亮点id",example = "1")
    private Integer id;
    @ApiModelProperty(value = "房源亮点")
    private String name;
}
