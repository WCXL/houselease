package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Ding on 2020/1/9.
 */
@ApiModel(value = "分页数据model")
public class PageLmit {
    @ApiModelProperty(value = "当前页", example = "1")
    private Integer pageNum;
    @ApiModelProperty(value = "每页条数", example = "1")
    private Integer pageSize;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        if (null != pageNum || pageNum != 0) {
            this.pageNum = pageNum;
        } else {
            this.pageNum = 1;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        if (null != pageSize || pageSize != 0) {
            this.pageSize = pageSize;
        } else {
            this.pageSize = 5;
        }
    }
}
