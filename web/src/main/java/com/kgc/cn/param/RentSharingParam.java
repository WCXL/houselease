package com.kgc.cn.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "合租model")
public class RentSharingParam implements Serializable {
    private static final long serialVersionUID = -4829083953448101823L;
    @ApiModelProperty(value = "房屋地址")
    private String houseAddress;
    @ApiModelProperty(value = "小区名称")
    private String houseName;
    @ApiModelProperty(value = "户型")
    private String apartment;
    @ApiModelProperty(value = "楼层",example = "1")
    private Integer floor;
    @ApiModelProperty(value = "卧室类型")
    private String bedroomType;
    @ApiModelProperty(value = "卧室朝向")
    private String bedroomOrientation;
    @ApiModelProperty(value = "房屋面积单位：平方米")
    private String houseArea;
    @ApiModelProperty(value = "装修情况表Id")
    private Integer decorateSituationId;
    @ApiModelProperty(value = "租赁日期id",example = "1")
    private Integer leaseId;
    @ApiModelProperty(value = "租赁单价",example = "1")
    private Integer leaseMoney;
    @ApiModelProperty(value = "联系人姓名")
    private String linkmanName;
    @ApiModelProperty(value = "联系人性别", example = "1")
    private Integer linkmanSex;
    @ApiModelProperty(value = "用户id",example = "1")
    private Integer userId;
    @ApiModelProperty(value = "看房时间")
    private String lookHouseTime;
    @ApiModelProperty(value = "适宜居住人数",example = "1")
    private Integer leaseHouseNumber;
    @ApiModelProperty(value = "租赁开始时间")
    private String leaseStartTime;
    @ApiModelProperty(value = "房屋配置：使用英文分号分割")
    private String homeAppliances;
    @ApiModelProperty(value = "房屋亮点：使用英文分号分割")
    private String houseBenefit;
    @ApiModelProperty(value = "租房要求，使用英文分号分割")
    private String leaseRequire;
    @ApiModelProperty(value = "备注")
    private String comment;
    @ApiModelProperty(value = "是否合租：0：整租；1：合租", example = "1")
    private Integer isJointRent;
}
