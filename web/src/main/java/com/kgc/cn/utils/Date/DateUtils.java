package com.kgc.cn.utils.Date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Ding on 2020/1/8.
 */
public class DateUtils {

    public static int getTimeDifferenceByDay(Long fromDateStr, Long toDateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String fromDate = sdf.format(fromDateStr);
        String toDate = sdf.format(toDateStr);
        long from = sdf.parse(fromDate).getTime();
        long to = sdf.parse(toDate).getTime();
        int days = (int) ((to - from) / (1000 * 60 * 60 * 24));
        return days;
    }

    public static int getTimeDifferenceByHour(Long fromDateStr, Long toDateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String fromDate = sdf.format(fromDateStr);
        String toDate = sdf.format(toDateStr);
        long from = sdf.parse(fromDate).getTime();
        long to = sdf.parse(toDate).getTime();
        int hours = (int) ((to - from) / (1000 * 60 * 60));
        return hours;
    }

    public static int getTimeDifferenceByMinutes(Long fromDateStr, Long toDateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        String fromDate = sdf.format(fromDateStr);
        String toDate = sdf.format(toDateStr);
        long from = sdf.parse(fromDate).getTime();
        long to = sdf.parse(toDate).getTime();
        int minutes = (int) ((to - from) / (1000 * 60));
        return minutes;
    }
}
